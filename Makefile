#!/bin/bash
COMPILE_FLAGS_COMMON=-Wall -Os -Wextra
INCLUDE_PETASENSE_LIB_HEADERS=product
PETASENSE_LIB_SOURCE_FILES=product/timestamp.cpp

$(info $(INCLUDE_PETASENSE_LIB_HEADERS))
$(info $(COMPILE_FLAGS_COMMON))

all: test_work
	echo "All done"


test_work: test_work.cpp
	g++ $(COMPILE_FLAGS_COMMON) -I$(INCLUDE_PETASENSE_LIB_HEADERS) test_work.cpp -o test_work


