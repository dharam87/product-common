/*
 * Timestamp related structures and methods
 */
#pragma once

#define SECONDS_IN_A_WEEK       604800
#define SECONDS_IN_A_DAY        86400
#define SECONDS_IN_AN_HOUR      3600
#define SECONDS_IN_A_MINUTE     60
#define DAYS_IN_A_WEEK          7
#define HOURS_IN_A_DAY          23 // 0-23
#define MINS_IN_AN_HOUR         60

typedef struct {
    uint8_t wday;
    uint8_t hour;
    uint8_t min;
    uint8_t sec;
} scheduledTime;

typedef enum {
    TS_DIRECTION_FUTURE = 0,
    TS_DIRECTION_PAST = 1,
} ts_dir_t;

typedef struct {
    time_t      startTs;
    time_t      endTs;
    uint32_t    interval;
} ScheduleInternal_t;

bool getUtcDate (time_t currentTime, scheduledTime &currentDate);
scheduledTime convert_timestring(const std::string &scheduleTimeStr);
time_t getTimestamp (time_t fromTs, scheduledTime &scheduleTime, ts_dir_t tsDir);

