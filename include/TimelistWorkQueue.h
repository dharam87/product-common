#include <cstdio>
#include <ctime>

#include "timestamp.h"
#include "Work.h"
#include "WorkQueue.h"

#pragma once

using std::queue;
using std::string;

class TimelistWorkQueue: public WorkQueue {
private:
    std::list<std::string>* timelist;
    std::list<time_t> schedule;
    std::queue<Work*> work_list;

    int list_max_size;
    time_t nextDue;


    void updateNextDue(void) {
        /* Calculate the next schedule and update in the list */
        schedule.push_back (schedule.front () + SECONDS_IN_A_WEEK);
        schedule.pop_front ();
        nextDue = schedule.front ();
    }

public:
#define TIMELIST_MAX_ENTRIES 400

    TimelistWorkQueue(std::list<std::string>* time_list):WorkQueue(WorkQueueType::WORKQUEUE_TYPE_TIMELIST), list_max_size(TIMELIST_MAX_ENTRIES){
        timelist = time_list;

        if (time_list->size () > list_max_size) {
            printf ("Too many discrete time (%lu) in time schedule, only %d are allowed\n", time_list->size (),
                  list_max_size);
            time_list->resize (list_max_size);
        }

        timelist = time_list;

        for(auto &it : *timelist){
            scheduledTime when = convert_timestring(it);
            time_t new_ts = getTimestamp(time(0), when, TS_DIRECTION_FUTURE);
            printf("%s calculated timestamp: %lu\n", __func__, new_ts);
            schedule.push_back(new_ts);
        }

        schedule.sort();
        printf("%s %d\n", __func__, __LINE__);
        enabled = false;
        nextDue = schedule.front();
    }

    TimelistWorkQueue(TimelistWorkQueueArgs args):WorkQueue(WorkQueueType::WORKQUEUE_TYPE_TIMELIST),list_max_size(TIMELIST_MAX_ENTRIES){

        if (args.timelist.size () > list_max_size) {
            printf ("Too many discrete time (%lu) in time schedule, only %d are allowed\n", args.timelist.size (),
                  list_max_size);
            args.timelist.resize (list_max_size);
        }

        timelist = &(args.timelist);
        for(auto &it : *timelist){
            scheduledTime when = convert_timestring(it);
            time_t new_ts = getTimestamp(time(0), when, TS_DIRECTION_FUTURE);
            printf("%s calculated timestamp: %lu\n", __func__, new_ts);
            schedule.push_back(new_ts);
        }

        schedule.sort();
        printf("%s %d\n", __func__, __LINE__);
        enabled = false;
        nextDue = schedule.front();
    }

    TimelistWorkQueue(TimelistWorkQueueArgs* args): WorkQueue(WorkQueueType::WORKQUEUE_TYPE_TIMELIST),list_max_size(TIMELIST_MAX_ENTRIES){

        if (args->timelist.size () > list_max_size) {
            printf ("Too many discrete time (%lu) in time schedule, only %d are allowed\n", args->timelist.size (),
                  list_max_size);
            args->timelist.resize (list_max_size);
        }

        timelist = &args->timelist;
        for(auto &it : *timelist){
            scheduledTime when = convert_timestring(it);
            time_t new_ts = getTimestamp(time(0), when, TS_DIRECTION_FUTURE);
            printf("%s calculated timestamp: %lu\n", __func__, new_ts);
            schedule.push_back(new_ts);
        }

        schedule.sort();
        printf("%s %d\n", __func__, __LINE__);
        enabled = false;
        nextDue = schedule.front();
    }

    bool compare_args(WorkQueueArgs* cmp_args) {
        if (!cmp_args) {
            return false;
        }

        TimelistWorkQueueArgs* new_args = dynamic_cast<TimelistWorkQueueArgs *>cmp_args;
        if (new_args->timelist == timelist) {
            return true;
        }

        return false;
    }

    void queue_work(Work* work) {
        if (work)
            work_list.push(work);
    }

    bool isEmpty(void) {
        return work_list.empty();
    }

    bool cancel_work(Work* work) { return false; }

    void enable(void) {
        if (enabled){
            return;
        }

        enabled = true;
        /* re-evaluate timestamps once workqueue is enabled(if it has been disabled) */
        if (schedule.size()==0 && timelist->size()!=0) {
            for(auto &it : *timelist){
                scheduledTime when = convert_timestring(it);
                time_t new_ts = getTimestamp(time(0), when, TS_DIRECTION_FUTURE);
                schedule.push_back(new_ts);
            }
            schedule.sort();
        }
        nextDue = schedule.front();
    }

    void disable(void) {
        enabled = false;
        schedule.clear();
    }

    void flush(void){}

    Work* nextDueWork() {
        return nullptr;
    }

    bool hasPendingWork(time_t current=std::time(0)) {
        if (!isEnabled() || isEmpty()) {
            printf("Queue is disabled or Empty\n");
            return false;
        }

        if (nextDue <= current) {
            return true;
        }

        return false;
    }

    void run() {
        // Runs all the works associated with this Queue
        if (!isEnabled()) {
            printf("Workqueue is disabled - can't execute any work!!\n");
            return;
        }
        if (!hasPendingWork())
        {
            printf("Nothing due on the Workqueue\n");
            return;
        }
        /* fetch initial size of the Queue before we run all the queued jobs
         * TODO: protect this with mutex - could be done with lists/vectors as well
         * might need to use lists/vectors for simplicity - to avoid pop/push
         */
        int queue_size = work_list.size();
        while (queue_size--) {
            /* pop the first element in queue */
            Work* q_work = work_list.front();
            work_list.pop();
            /* do the core work */
            q_work->do_work();
            /* push it back to the queue as it is scheduled periodic work - this should not impact queue_size */
            work_list.push(q_work);
        }

        updateNextDue();
    }


    time_t DueTime() { return nextDue; }

    int DueTimeInseconds() {return (nextDue - time(0));}
};
