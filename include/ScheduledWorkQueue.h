#include <ctime>
#include <cstdio>
#include <queue>

#include"timestamp.h"
#include "Work.h"
#include "WorkQueue.h"

#pragma once

using std::string;
using std::queue;

class ScheduledWorkQueue: public WorkQueue {

private:
    std::queue<Work*> work_list;

    std::string start_time_str;
    std::string end_time_str;
    uint32_t interval;

    scheduledTime start;
    scheduledTime end;
    ScheduleInternal_t schedule;

    time_t last_occured_time;
    time_t nextDue;

    /* advance the next-due time as per the end-time */
    void advance(time_t &nextMeasurementTs);

    /* resets the next-due as per current-time and with start-time and end-time as reference.
     * for example - use this to fetch new timings if the queue has been in disabled state and has been
     * enabled after some considerable time.
     */
    void process(time_t currentTimestamp=time(0));

    /* wrapper for updating the next Due time */
    void updateNextDue(void);

public:
    ScheduledWorkQueue(std::string stime, std::string etime, uint32_t interval);

    ScheduledWorkQueue(ScheduledWorkQueueArgs args);

    ScheduledWorkQueue(ScheduledWorkQueueArgs* args);

    bool compare_args(WorkQueueArgs* cmp_args);

    void enable(void);

    void disable(void);

    void queue_work(Work* new_work);

    bool isEmpty(void);

    bool hasPendingWork(time_t current=std::time(0));

    void run(void);

    bool cancel_work(Work& work_obj);

    void flush(void);

    Work* nextDueWork();

    time_t DueTime();

    int DueTimeInseconds() {return (nextDue - time(0));}
};
