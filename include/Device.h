/*
 * Define structures and interface for Sensors and channels
 */

#pragma once

enum class SignalType
{
  analog,
  digital,
  invalid
};

enum class SensorType
{
  analog,
  vibration,
  magnetometer,
  temperature,
  ultrasound,
  pressure,
  current,
  invalid
};

enum class SensorDriver
{
  vsxtemperature,
  none
};

typedef uint32_t sensor_id_t;
typedef uint32_t channel_id_t;
typedef uint32_t device_id_t;
typedef uint32_t rule_id_t;
typedef uint32_t machine_id_t;

enum class ChannelDataType
{
  Vibration_Accel_x, /* 0 */
  Vibration_Accel_y, /* 1 */
  Vibration_Accel_z, /* 2 */
  Vibration_Accel_xyz,

  Vibration_Velocity_x,
  Vibration_Velocity_y,
  Vibration_Velocity_z,
  Vibration_Velocity_xyz,

  Magnetic_Field_x,
  Magnetic_Field_y,
  Magnetic_Field_z,
  Magnetic_Field_xyz,

  Temperature_magnitude,
  Temperature_waveform,

  Generic_Analog,

  Invalid = 1024,
};

enum class MeasurementType {
  /* any waveform sensor data without any processing done on it */
  Raw_Waveform,
  /* waveform data with some filtering-frequency settings applied to it, if specified */
  FullBandwidth_Waveform,
  /* waveform data with higher resolution - differnet settings - if specified */
  HighRes_Waveform,
  /* Fetch Overall(summary) information for the sensor, parameters may change for different sensor type */
  Overall,
  /* check if the Machine is being Utilized or Not - will be used for Event-Detection.
   * ex- Machine is ON -> do a P2P Sync; Machine is OFF -> Disable all sensor activities
   * For Magnetometers, it will by default use z-axis(Magnetic_Field_z) as Channel data type and
   * should be handled as Event-detection settings.
   * For Vibration Sensors also, it can be default to use z-axis(Vib_Accel_z) as Channel Data-type
   * and should be handled as Event-detection Settings.
   * This Measurement-type may have additional parameters like threshold associated with chanel data-type
   * being measured.
   */
  Utilization,
  /* evaluate Arithmetic Mean for the collected data */
  Mean,
  /* evaluate Median for the data */
  Median,
  /* evaluate RootMeansquare for the data */
  RootMeanSquare,
  /* Peak */
  Peak,
  /* Peak to Peak */
  PeaktoPeak,
  /* standard Deviation for the data */
  Standard_Deviation,
  /* crest factor for the measured data */
  Crest_Factor,
  /* spectrum peak */
  Spectrum_Peak_1x,

  Invalid= 1024,
};

typedef struct {
    uint32_t size;
    uint32_t rate;
} SamplingConfig;

typedef struct {
    /* signal tpe - digital or analog */
    SignalType signal_type;
    SensorDriver driver;
    union {
      struct {
          bool loadResistorEnabled;
          bool powerConfigAvailable;
          bool boostConverterEnabled;
          uint8_t powerConfig;
          uint8_t gainFactor;
          uint8_t bias;
          uint16_t inputRange;
          uint16_t filterFrequency;
          uint16_t settlingTime;
          uint16_t operatingVoltage;
          float sensitivity;
      } analog;
      struct {
          uint16_t filterFrequency;
          uint8_t fs_range;
          float sensitivity;
      } digital;
    } sensor;
} SensorSettings;

enum class ChannelP2PRole {
    P2P_NONE,
    P2P_SLAVE,
    P2P_MASTER
};

typedef struct {
    /* cloud-generated id of the channel */
    channel_id_t  channel_id;

    /* which port the channel is on the device - (0,2) for Transmitters, 0 for VM3s */
    uint8_t port_idx;

    /* channel idx on the device  - (0, 8) for Transmitters, (0,3) for VM3s */
    uint8_t channel_idx;

    /* specify physical properties the channel mesure
     */
    SensorType channel_sensor_type;

    /*
     * Specify any custom driver to invoke while processing this channel
     */
    SensorDriver driver;

    /* what data-types the channel is configured to use
     * TODO: for now - make it single value but does not have to be
     */
    ChannelDataType data_type;

    struct {
      bool use_scheduled;
      bool p2p_enabled;
      bool has_event_detection;
    } flags;

    ChannelP2PRole p2p_role;

    /* any work on this channel following a scheduled - queued to Scheduled Workqueue */
    std::unordered_set<Work*> scheduled_work;
    /* any work on this channel using StateBased Workqueue */
    std::unordered_set<Work*> state_work;
    /* any work on this channel doing event detection - might be queued to Scheduled Workqueue */
    std::unordered_set<Work*> event_detection_work;

} ChannelAttributes;

typedef struct sensorAttributes {
    /* cloud-generated id of the sensor */
    uint32_t  sensor_id;
    /* Mask of channels(from 1<<0 to 1<<8) for each channel on the port */
    uint16_t  channelMask;

    /* name of the sensor */
    std::string name;

    /* specify physical properties the sensor mesure - if composite(for example - vibration + temperature),
     * specify all of them.
     */
    std::unordered_set<SensorType> sensor_type;

    /* maximum channels this sensor has */
    int max_channels;

    /* map of channels and channel attributes associated with this channel */
    std::map<channel_id_t, ChannelAttributes> channels;

    /* specify any custom driver the sensor will use */
    SensorDriver driver;

    /* Default Sensor settings */
    SensorSettings default_settings;
} sensorAttr_t;

class SensorMap {
  std::map<sensor_id_t, sensorAttr_t> sensorMap;
public:
  bool has_sensor(sensor_id_t sid) {
    if (sensorMap.find(sid) == sensorMap.end()) {
      return false;
    }
    return true;
  }

  bool getSensorAttr(sensor_id_t sid, sensorAttr_t& value) {
    if (has_sensor(sid)) {
      value = sensorMap[sid];
      return true;
    }
    return false;
  }

  uint16_t get_channel_mask(sensor_id_t sid) {
    if (has_sensor(sid)) {
      return sensorMap[sid].channelMask;
    }
    return 0;
  }

  bool has_channel(sensor_id_t sid, uint16_t channelidx) {
    if (has_sensor(sid)) {
       if ( (1<<channelidx) & (sensorMap[sid].channelMask) ) {
        return true;
       }
    }
    return false;
  }

  void dump(void) {
      for (auto &&it: sensorMap) {
        printf ("cloudSensorId: %u, deviceSensorId: %u, channelMask %u ", it.first,
              it.second.sensor_id,it.second.channelMask);
        printf("sensorTypes: ");
        for(auto stype:it.second.sensor_type)
        {
          printf("%d ", stype);
        }
        std::map<channel_id_t, ChannelAttributes> channels = it.second.channels;
        for(auto c:channels)
        {
          printf ("cloudChannelId: %u, PortIndx: %u, channelIndx %u sensorType: %d, ", c.first,
              c.second.port_idx,c.second.channel_idx, c.second.channel_sensor_type);

          printf("p2p_flag: %s, ", c.second.flags.p2p_enabled ? "true" : "false");
          printf("scheduled_flag: %s, ", c.second.flags.use_scheduled ? "true" : "false");
          printf(" event_detection_flag: %s ",c.second.flags.has_event_detection ? "true" : "false");
        }

        printf("\n");
      }
  }

  bool empty(void) {
    return sensorMap.empty();
  }

  std::map<sensor_id_t, sensorAttr_t>& getMapHandle(void) {
    return sensorMap;
  }
};

class Machine {
#define P2P_CONFIG_MAX_DEVICES              (20)

  /* current Machine state: ON or OFF */
  bool machine_state;
  /* how much time Machine has been 'ON' - in seconds */
  uint32_t machine_on_duration;
  /* how much time machine has been 'OFF' - in seconds */
  uint32_t machine_off_duration;

  /* time when machine state changed */
  time_t state_changed_time;

  struct {
    /* time when heartbeat sync was triggered between all devices of the Machine - applicable for p2p protocol only */
    time_t last_occured;

    /* time-period when the master device has to heart-beat sync with other sensors on this machine.*/
    uint32_t sync_period;

    /* heartbeat sync loss count so far */
    uint32_t sync_failed_count;

    /* Max failed heart-beats allowed */
    uint32_t sync_failed_max;

    /* Maximum duration with heartbeat sync failing - as received from the Cloud */
    uint32_t sync_failed_max_duration;
  } p2p_heartbeat;

  struct {
    device_id_t dev_id;
    std::string dev_serial_num;
    std::string bd_addr;
    int tdm_slot;
  } p2p_machine_config[P2P_CONFIG_MAX_DEVICES];

  /*
   * One sensor can be only attached to one Machine - Sensors won't
   * jump across different machine(assets)
   */
  std::list<sensor_id_t> sensor_list;
public:
  void setMachineState(bool on_off, time_t now);
  bool getMachineState(void) const;

  uint32_t getMachineOnDuration(void) const;
  uint32_t getMachineOffDuration(void) const;

  bool getMachineId(void) const;

  bool setHeartbeatTimeoutduration(uint32_t max_duration);
  bool getHeartbeatTimeoutDuration(uint32_t max_duraton);
  bool isP2PMasterOffline(void);
  bool isHeartBeatdue(void);
  void tickHeartBeat(time_t now);

  void setHeartbeatSyncPeriod(uint32_t sync_interval);
  uint32_t getHeartbeatSyncPeriod(void);
};

enum DeviceMode {
  /* In this mode, Petasense devices will collect data from sensors and will communicate
   * with the cloud directly. They may also receive commands from another sensor(ex - P2P sync)
   * for taking measurements. Sensor devices may receive commands from nearby Hubs(over Bluetooth)
   * as well. Both VM3s and Transmitters can run as Sensor Mode
   */
  DEVICE_MODE_SENSOR,
  /* In this mode, Petasense devices will not collect any data by itself but will instruct
   * other sensors to take readings and pass it back to the Gateway. There can be only one Gateway
   * per asset but same Gateway can be used for multiple assets
   * Gateway devices will always be Powered-ON and will always be ready to receive Async commands
   * from Cloud/User(remotely). Remote User can pass instructions for a given machine or group of sensors
   * which are under the given Gateway control. The sensors which are part of the Gateway group may
   * send data back to Cloud via Gateway(immediately!).
   *
   * Hence, Only PoE-d Transmitters or similar devices can act as Gateway.
   */
  DEVICE_MODE_GATEWAY,
  /* Act as both Sensor and Gateway. Applicable only for PoE-d transmitters.
   * The Transmitter(Gateway) will collect data from the sensors attached to it as well as
   * take commands from Remote User - pass the same to other sensors in the group to collect data
   * or pass configuration changes etc.
   */
  DEVICE_MODE_HYBRID,
};

enum class DeviceModel {
  TRANSMITTER,
  MOTE // Generation-2 motes i.e. VM3 and onwards
};

enum class ModelHwVersion {
  TRANSMITTER_0804,
  TRANSMITTER_0805,
  MOTE_0901,
};

class MachineRule {
    rule_id_t rule_id;
    std::string rule_name;
    bool enabled;
};

class PetasenseDevice {
    static DeviceMode mode;
    static DeviceModel model;

    device_id_t devid;
    std::string dev_serial_num;
    std::string bd_addr;

    /* Map of associated machines */
    std::map<machine_id_t, Machine> associated_machines;
    /* Defined machine rules per machine */
    std::map<machine_id_t, MachineRule> rules_db;
    SensorMap sensors_db;
    WorkQueueManager wq_manager;


    /* system-wide - is P2P enabled - if yes, it applies for all the channels associated with the device */
    bool p2p_enabled;

public:
    PetasenseDevice(device_id_t devid, std::string serial_number, DeviceMode mode=DEVICE_MODE_SENSOR) {

    }

    ~PetasenseDevice();

    static DeviceModel getDeviceModel() const {
        return model;
    }

    static void setDeviceModel(DeviceModel _model) {
        model = _model;
    }
};
