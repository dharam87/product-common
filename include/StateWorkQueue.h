#include <cstdio>
#include <ctime>
#include<queue>

#include"timestamp.h"
#include "Work.h"
#include "WorkQueue.h"

#pragma once

using std::queue;
using std::string;

class StateWorkQueue: public WorkQueue {

private:

    std::queue<Work*> work_list;
    const int STATE_BASED_MODE_AGGRESSIVE = 0;
    const int STATE_BASED_MODE_NORMAL = 1;

    int mode;

    StateWorkQueueArgs schedule;
    time_t nextDue;

    bool state;

    void process(time_t currentTimestamp=time(0))
    {
    }

    void updateNextDue(void) {
        if (mode == STATE_BASED_MODE_NORMAL) {
            nextDue += schedule.params.normal.interval;
        }
        else if (mode == STATE_BASED_MODE_AGGRESSIVE) {
            /* to be implemented */
        }
        else {
            /* handle */
        }
    }

    void set_state(bool on_off) {
        state = on_off;
    }

    bool get_state(void) const {
        return state;
    }

public:
    StateWorkQueue(uint32_t interval): WorkQueue(WorkQueueType::WORKQUEUE_TYPE_STATEBASED){
        printf("%s %d\n", __func__, __LINE__);
        schedule.params.normal.interval = interval;
        mode = STATE_BASED_MODE_NORMAL;
        state = false;
    }

    StateWorkQueue(uint32_t msmt_timeout, uint32_t msmt_interval, uint32_t nr_readings_per_day, uint32_t reading_interval):WorkQueue(WorkQueueType::WORKQUEUE_TYPE_STATEBASED) {
        mode = STATE_BASED_MODE_AGGRESSIVE;
        // schedule.aggressive.measurement_timeout = msmt_timeout;
        // schedule.aggressive.measurement_interval = msmt_interval;
        // schedule.aggressive.reading_interval = reading_interval;
        // schedule.aggressive.max_readings_per_day = nr_readings_per_day;
        state = false;
    }

    StateWorkQueue(StateWorkQueueArgs args):WorkQueue(WorkQueueType::WORKQUEUE_TYPE_STATEBASED) {
        mode = args.mode;
        if (args.mode == STATE_BASED_MODE_AGGRESSIVE) {
            state = false;
        }
        else if (args.mode == STATE_BASED_MODE_NORMAL) {
            state = false;
            schedule.params.normal.interval = args.params.normal.interval;
        }
    }

    StateWorkQueue(StateWorkQueueArgs* args):WorkQueue(WorkQueueType::WORKQUEUE_TYPE_STATEBASED) {
        mode = args->mode;
        if (args->mode == STATE_BASED_MODE_AGGRESSIVE) {
            state = false;
        }
        else if (args->mode == STATE_BASED_MODE_NORMAL) {
            state = false;
            schedule.params.normal.interval = args->params.normal.interval;
        }
        else {

        }
    }

    bool compare_args(WorkQueueArgs* cmp_args) {
        if (!cmp_args) {
            return false;
        }

        StateWorkQueueArgs* new_args = dynamic_cast<StateWorkQueueArgs *>cmp_args;
        if(new_args->mode == STATE_BASED_MODE_NORMAL) {
            if (schedule.params.normal.interval == new_args.params.normal.interval) {
                return true;
            }
        }
        return false;
    }

    void enable(void) {
        enabled = true;
        nextDue = time(0);
        /* if the Workqueue is being disabled/enabled - we can't assume the state is still ON */
        state = false;
    }

    void disable(void) {
        enabled = false;
    }

    void queue_work(Work* new_work) {
        if (new_work)
            work_list.push(new_work);
    }

    bool isEmpty(void) {
        return work_list.empty();
    }

    bool hasPendingWork(time_t current=std::time(0)) {
        if (!isEnabled() || isEmpty()) {
            printf("Queue is disabled or Empty\n");
            return false;
        }

        if (mode == STATE_BASED_MODE_NORMAL) {
            get_state();
            if (nextDue <= current) {
                return true;
            }
        }
        else if (mode == STATE_BASED_MODE_AGGRESSIVE) {

        }


        return false;
    }

    void run(void) {
        // Runs all the works associated with this Queue
        if (!isEnabled()) {
            printf("Workqueue is disabled - can't execute any work!!\n");
            return;
        }

        /* if state is OFF */
        if (!get_state()) {
            return;
        }

        if (!hasPendingWork())
        {
            printf("Nothing due on the Workqueue\n");
            return;
        }

        /* fetch initial size of the Queue before we run all the queued jobs
         * TODO: protect this with mutex - could be done with lists/vectors as well
         * might need to use lists/vectors for simplicity - to avoid pop/push
         */
        int queue_size = work_list.size();
        while (queue_size--) {
            /* pop the first element in queue */
            Work* q_work = work_list.front();
            work_list.pop();
            /* do the core work */
            q_work->do_work();
            /* push it back to the queue as it is scheduled periodic work - this should not impact queue_size */
            work_list.push(q_work);
        }

        updateNextDue();
    }

    bool cancel_work(Work& work_obj) { return true; }

    void flush(void) { }

    Work* nextDueWork() { return 0; }

    time_t DueTime() {return nextDue; }

    int DueTimeInseconds() {return (nextDue - time(0));}
};
