/*
 * Defines One-shot Workqueues - holding jobs which need to be done once !
 * The work can be delayed by 'x' seconds. It reduces to Instant Workqueue -
 * when x =0, required for handling second-half of Interrupt requests.
 */
#include <ctime>
#include <cstdio>

#include "timestamp.h"
#include "Work.h"
#include "WorkQueue.h"

#pragma once

class OneshotWorkQueue: public WorkQueue {
    struct OneshotWork {
        /* Use this field for Instantenous works(delay=0) - which need to be executed as soon as possible */
        time_t  inserted_at;
        /* time in future when this work is to be executed */
        time_t  deadline;
        /* actual work object */
        Work*   work_obj;

        friend bool operator< (OneshotWork const& lhs, OneshotWork const& rhs) {
            if (lhs.deadline == rhs.deadline) {
                return lhs.inserted_at > rhs.inserted_at;
            }
            return lhs.deadline > rhs.deadline;
        }
    };
    std::priority_queue<struct OneshotWork> schedule;
    time_t nextDue;

    void updateNextDue() {
        nextDue = schedule.top().deadline;
        printf("%s %d nextDue: %lu\n", __func__,__LINE__, nextDue);
    }
public:
    OneshotWorkQueue():WorkQueue(WorkQueueType::WORKQUEUE_TYPE_ONESHOT) {
    }

    void queue_work_delayed(Work* work, int delay_in_seconds=0) {
        if (!work) {
            return;
        }

        time_t inserted_at = time(0);
        time_t deadline = inserted_at + delay_in_seconds;
        OneshotWork t = {inserted_at, deadline, work};
        printf("%s work queued\n",__func__);
        schedule.push(t);
    }

    void queue_work(Work* work) {
        if (!work)
            return;
        queue_work_delayed(work);
    }

    bool isEmpty(void) {
        return schedule.empty();
    }

    bool cancel_work(Work* work) { return false; }

    void enable(void) {
        if (enabled){
            return;
        }

        enabled = true;
        if (schedule.empty()) {
            return;
        }

        nextDue = schedule.top().deadline;
    }

    void disable(void) {
        enabled = false;
    }

    void flush(void){}

    Work* nextDueWork() {
        if (isEmpty()) {
            return 0;
        }
        return 0;
    }

    bool hasPendingWork(time_t current=std::time(0)) {
        if (!isEnabled() || isEmpty()) {
            return false;
        }

        if (nextDue <= current) {
            return true;
        }

        return false;
    }

    void run() {
        // Runs all the works associated with this Queue
        if (!isEnabled()) {
            printf("Workqueue is disabled - can't execute any work!!\n");
            return;
        }
        if (!hasPendingWork())
        {
            printf("Nothing due on the Workqueue\n");
            return;
        }
        /*
         *
         */
        int queue_size = schedule.size();
        while (hasPendingWork()) {
            /* pop the first element in queue */
            Work* q_work = schedule.top().work_obj;
            schedule.pop();
            /* do the core work */
            q_work->do_work();
            printf("%s %d Work completed\n", __func__,__LINE__);
            updateNextDue();
        }
    }

    time_t DueTime() { return nextDue; }

    int DueTimeInseconds() {return (nextDue - time(0));}
};
