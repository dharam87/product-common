/*
 * Defines Work Interface and structures
 */

/* source of work

Scenario#1 p2p sync
Initial case: Event detection schedule -> Machine is ON -> trigger_p2p_measurement -> sync with the peers -> sync-done -> take_measurement-local
Aggressive MI : Machine status ON -> P2P Schedule Workqueue is set to active -> Execute Measurement jobs -> sync with the peers -> take_measurement_local;

Scenario#2 Bluetooth WiFi Reconfig request
1. Interrupt from Bluetooth -> Queue the WIFI_RECONFIG work on Instant-Workqueue -> run WorkqueueManager -> run the wifi-reconfig-work -> connect to imp-> pass wifi params
   -> wait for Connection status and WiFi Info -> pass the Information back on Bluetooth connected handle
*/
#pragma once

enum class ActionType {
    MEASURE_SENSOR,
    MEASURE_BATTERY,

    WIFI_CONNECT,
    WIFI_RECONFIG,
    NETWORK_SEND,
    NETWORK_RECEIVE,

    P2P_SYNC_MEASUREMENT,
    P2P_SYNC_HEARTBEAT,

    ACTION_NOT_DEFINED = 1024,
};

class WorkArgs {
};


typedef void (*WorkCallback)(WorkArgs * args);
typedef uint32_t work_handle_t;
class Work {
private:
    ActionType      action;
    WorkArgs*       args;
    WorkCallback    cb;
public:
    /* use this Factory method later to create different types of work. For now, keep it simple */
    static Work* create_work(WorkArgs* arguments, WorkCallback cb, ActionType action) {
        return new Work(arguments, cb, action);
    }

    Work(WorkArgs* arguments, WorkCallback cb, ActionType action_id): action(action_id), args(arguments), cb(cb) {

    }

    virtual void do_work(void) {
        if (cb) {
            cb(args);
        }
        else {
            printf("Work-Function not defined\n");
        }
    }
};

