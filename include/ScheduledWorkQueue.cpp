#include <ctime>
#include <cstdio>
#include <queue>

#include "timestamp.h"
#include "Work.h"
#include "WorkQueue.h"
#include "ScheduledWorkQueue.h"

using std::string;
using std::queue;

void ScheduledWorkQueue::advance(time_t &nextMeasurementTs)
{
  time_t nextStartTs, nextEndTs, nextNextMeasurementTs;

  // Get next timestamp from current end ts
  nextStartTs = getTimestamp (schedule.endTs, start, TS_DIRECTION_FUTURE);

  if (nextStartTs == schedule.endTs) {
    // next interval starts immediately as this one ends
    nextEndTs = getTimestamp (schedule.endTs + SECONDS_IN_A_MINUTE, start, TS_DIRECTION_FUTURE);
    if (nextMeasurementTs == nextStartTs) {
      // we have already taken a measurement at the start_ts of the next interval
      nextNextMeasurementTs = nextStartTs + schedule.interval;
    } else {
      nextNextMeasurementTs = nextStartTs;
    }
  } else {
    // there is a gap between the end of the previous interval and the start of the next
    nextEndTs = getTimestamp (nextStartTs, end, TS_DIRECTION_FUTURE);
    nextNextMeasurementTs = nextStartTs;
  }

  // Update next measurement timestamps
  schedule.startTs = nextStartTs;
  schedule.endTs = nextEndTs;
  nextMeasurementTs = nextNextMeasurementTs;
}

void ScheduledWorkQueue::process(time_t currentTimestamp=time(0))
{
    schedule.startTs = getTimestamp (currentTimestamp, start, TS_DIRECTION_PAST);
    schedule.endTs = getTimestamp (currentTimestamp, end, TS_DIRECTION_PAST);
    printf("%s: Initial starTS %lu endTS: %lu current:%lu\n", __func__,schedule.startTs, schedule.endTs,currentTimestamp);
    time_t nextMeasurementTs;
    if (schedule.endTs <= schedule.startTs) { // we are in the middle of the interval
        // Schedule has already started. find the next end time.
        schedule.endTs = getTimestamp (currentTimestamp, end, TS_DIRECTION_FUTURE);
        printf("%s: Re-evaluate: endTS: %lu current:%lu\n", __func__,schedule.endTs,currentTimestamp);

        nextMeasurementTs = schedule.startTs;
        while ((nextMeasurementTs < currentTimestamp) && (nextMeasurementTs <= schedule.endTs)) {
            // looking for next measurement time that lies within the interval
            nextMeasurementTs += schedule.interval;
        }
        if (nextMeasurementTs > schedule.endTs) {
        // no more measurements possible in this interval
            nextMeasurementTs -= schedule.interval;
            advance(nextMeasurementTs);
        }
    }
    else
    {
        // the interval defined by the previous start_ts has ended
        schedule.startTs = getTimestamp (currentTimestamp, start, TS_DIRECTION_FUTURE);
        schedule.endTs = getTimestamp (currentTimestamp, end, TS_DIRECTION_FUTURE);
        printf("%s Re-evaluating starTS %lu endTS: %lu current:%lu\n", __func__,schedule.startTs, schedule.endTs,currentTimestamp);
        nextMeasurementTs = schedule.startTs;
    }
    nextDue = nextMeasurementTs;
}

void ScheduledWorkQueue::updateNextDue(void) {
    nextDue += schedule.interval;
    if (nextDue > schedule.endTs) {
        nextDue -= schedule.interval;
        advance(nextDue);
    }
}

ScheduledWorkQueue::ScheduledWorkQueue(std::string stime, std::string etime, uint32_t interval):WorkQueue(WorkQueueType::WORKQUEUE_TYPE_SCHEDULE) {
    printf("%s %d\n", __func__, __LINE__);
    start_time_str = stime;
    end_time_str = etime;
    start= convert_timestring(start_time_str);
    end = convert_timestring(end_time_str);
    schedule.interval = interval;
}

ScheduledWorkQueue::ScheduledWorkQueue(ScheduledWorkQueueArgs args):WorkQueue(WorkQueueType::WORKQUEUE_TYPE_SCHEDULE) {
    printf("%s %d\n", __func__, __LINE__);
    start_time_str = args.start_time;
    end_time_str = args.end_time;
    start= convert_timestring(start_time_str);
    end = convert_timestring(end_time_str);
    schedule.interval = args.interval;
}


ScheduledWorkQueue::ScheduledWorkQueue(ScheduledWorkQueueArgs* args):WorkQueue(WorkQueueType::WORKQUEUE_TYPE_SCHEDULE) {
    printf("%s %d\n", __func__, __LINE__);
    start_time_str = args->start_time;
    end_time_str = args->end_time;
    start= convert_timestring(start_time_str);
    end = convert_timestring(end_time_str);
    schedule.interval = args->interval;
}

bool ScheduledWorkQueue::compare_args(WorkQueueArgs* cmp_args) {
    if (!new_args) {
        return false;
    }
    ScheduledWorkQueueArgs* new_args = dynamic_cast<ScheduledWorkQueueArgs *>cmp_args;
    if ( (new_args->start_time_str == this->start_time_str) && (new_args->end_time_str == this->end_time_str)
        && (new_args->interval = this->schedule.interval)) return true;

    return false;
}

void ScheduledWorkQueue::enable(void) {
    enabled = true;
    if (!isEmpty()) {
        process(std::time(0));
    }
}

void ScheduledWorkQueue::disable(void) {

}

void ScheduledWorkQueue::queue_work(Work* new_work) {
    if (new_work)
        work_list.push(new_work);
}

bool ScheduledWorkQueue::isEmpty(void) {
    return work_list.empty();
}

bool ScheduledWorkQueue::hasPendingWork(time_t current=std::time(0)) {
    if (!isEnabled() || isEmpty()) {
        printf("Queue is disabled or Empty\n");
        return false;
    }

    if (nextDue <= current) {
        return true;
    }

    return false;
}

void ScheduledWorkQueue::run(void) {
    // Runs all the works associated with this Queue
    if (!isEnabled()) {
        printf("Workqueue is disabled - can't execute any work!!\n");
        return;
    }
    if (!hasPendingWork())
    {
        printf("Nothing due on the Workqueue\n");
        return;
    }
    /* fetch initial size of the Queue before we run all the queued jobs
     * TODO: protect this with mutex - could be done with lists/vectors as well
     * might need to use lists/vectors for simplicity - to avoid pop/push
     */
    int queue_size = work_list.size();
    while (queue_size--) {
        /* pop the first element in queue */
        Work* q_work = work_list.front();
        work_list.pop();
        /* do the core work */
        q_work->do_work();
        /* push it back to the queue as it is scheduled periodic work - this should not impact queue_size */
        work_list.push(q_work);
    }

    updateNextDue();
}

bool ScheduledWorkQueue::cancel_work(Work& work_obj) { return true; }

void ScheduledWorkQueue::flush(void) { }

Work* ScheduledWorkQueue::nextDueWork() { return 0; }

time_t ScheduledWorkQueue::DueTime() {return nextDue; }

int ScheduledWorkQueue::DueTimeInseconds() {return (nextDue - time(0));}