/*
 * WorkQueue class definition
 * author: dharam@petasense.com
 */
#pragma once

using std::string;

enum class WorkQueueType {
    WORKQUEUE_TYPE_SCHEDULE,
    WORKQUEUE_TYPE_TIMELIST,
    WORKQUEUE_TYPE_ONESHOT,
    WORKQUEUE_TYPE_STATEBASED,

    WORKQUEUE_TYPE_INVALID,
};

class WorkQueueArgs {
    /* make this class virtual so that all casting can be safe using dynamic_cast */
    virtual void dummy(void) {};
};

class ScheduledWorkQueueArgs: public WorkQueueArgs {
public:
    std::string start_time;
    std::string end_time;
    uint32_t interval;
    ScheduledWorkQueueArgs(std::string s, std::string e, uint32_t t) {
        start_time = s;
        end_time = e;
        interval = t;
    }
};

class TimelistWorkQueueArgs: public WorkQueueArgs {
public:
    std::list<std::string> timelist;
};

class StateWorkQueueArgs: public WorkQueueArgs {
public:
    int mode;
    union {
        struct {
            time_t reset_counters_time;
            bool reading_in_progress;
            uint32_t measurement_timeout;
            uint32_t measurement_interval;
            uint32_t reading_interval;
            uint32_t max_readings_per_day;
            uint32_t count_readings;
            uint32_t count_measurements;
        } aggressive;

        struct {
            uint32_t interval;
        } normal;
    } params;
};

class WorkQueue {
private:
    WorkQueueType wq_type;
public:
    bool enabled; // enabled or disabled;

    WorkQueue(WorkQueueType t) {
        wq_type = t;
    }
    virtual bool cancel_work(Work* work_obj) {return true;}

    virtual void queue_work(Work* new_work) { }

    virtual void enable(void) {}

    virtual void disable(void) {}

    virtual void flush(void){}

    virtual Work* nextDueWork() {return 0;}

    /* @output earliest deadline for all the queued work */
    virtual time_t DueTime() {return 0;}

    int DueTimeInseconds() {return -1;}

    virtual bool hasPendingWork(time_t curtime) {return false;}
    /* if any work is pending - run it */
    virtual void run() {printf("%s %d\n", __func__,__LINE__);}

    virtual bool isEmpty(void) { return false; }

    bool isEnabled(void) {
        return enabled;
    }

    WorkQueueType getType(void) const {
        return wq_type;
    }
    /* Equality check - see if the given Workqueue has same parameters or not */
    virtual bool compare_args(WorkQueueArgs* new_args) {
        return false;
    }

    virtual ~WorkQueue() {}
};


class WorkQueueManager {
    std::unodered_set<WorkQueue *> wqs;
public:
    /* factory method to create workqueues */
    static WorkQueue* create_workqueue(WorkQueueArgs* p, WorkQueueType qtype);

    void add_workqueue(WorkQueue*  wq);

    void remove_workqueue(WorkQueue* wq);

    WorkQueue* find_workqueue(WorkQueueArgs* p, WorkQueueType qtype);

    void run(void);

    time_t nextDueTime(void);
};
