/*
 * Define Measurement Work class
 */
#include "Work.h"

#pragma once

struct channelMeasurmentWorkArgs: public WorkArgs
{
    channel_id_t    cid;
    device_id_t     dev_id;
    SensorDriver    driver;
    uint8_t         channel_idx;
    ChannelDataType data_type;
    MeasurementType meas_type;

    SamplingConfig  sampling;
    SensorSettings  settings;
};

typedef void (*MeasurementWorkCallback)(channelMeasurmentWorkargs args);

class MeasurementWork: public Work {
private:
    /* generic callback - if the constructor does not specify it */
    static MeasurementWorkCallback generic_cb;
public:
    MeasurementWork(channelMeasurmentWorkArgs* args, MeasurementWorkCallback cb=generic_cb):Work((WorkArgs*)args, (WorkCallback)cb, action::MEASURE_SENSOR) {
    }
    /* set a global callback for all measurement works */
    static void setMeasurementWorkCallback(MeasurementWorkCallback cb) {
        generic_cb = cb;
    }
};

