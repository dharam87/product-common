/* Definition of Work, WorkQueues and other scheduling related classes */

#include<iostream>
#include<unistd.h>
#include<cstdio>

#include "Work.h"

typedef struct work_arg1 {
    int a;
} workarg1;

typedef void (*work_func_1)(workarg1 a);
void work_func1(workarg1 a) {
    printf("%s %d %d\n",__func__, __LINE__, a.a);
}

typedef struct work_arg2 {
    char c;
} workarg2;

typedef void (*work_func_2)(workarg2 c);

void work_func2(workarg2 c) {
    printf("%s %d %c\n",__func__, __LINE__, c.c);
}

typedef struct work_arg3 {
    float d;
    std::string foo;
} workarg3;

typedef void (*work_func_3)(workarg3 d);

void work_func3(workarg3 d) {
    printf("%s %d %f str: %s\n",__func__, __LINE__,d.d, d.foo.c_str());
}


void test_method_create_work(void) {
    workarg1 w1 = {5};
    Work<workarg1, work_func_1, action_t>* work1 = Work<workarg1, work_func_1>::create_work(w1, work_func1, MEASURE_SENSOR);
    work1->do_work();

    workarg2 w2 = {'a'};
    Work<workarg2, work_func_2, action_t>* work2 = Work<workarg2, work_func_2>::create_work(w2, work_func2, MEASURE_SENSOR);
    work2->do_work();

    workarg3 w3 = {99.99, "hello"};
    Work<workarg3, work_func_3, action_t>* work3 = Work<workarg3, work_func_3>::create_work(w3, work_func3,MEASURE_SENSOR);
    work3->do_work();
}

void test_constructor_work(void) {
    workarg1 w1 = {1234};
    auto* wrk = new Work<workarg1, work_func_1>(w1, work_func1, ACTION_NOT_DEFINED);
    wrk->do_work();
}

typedef void (*work_cb_reference)(workarg1& a);

void work_callback_by_reference(workarg1& a) {
    a.a += 1000;
    printf("%s %d %d\n",__func__, __LINE__, a.a);
}

void test_args_as_reference(void) {
    workarg1 w = {10};
    auto* wrk = new Work<workarg1 &, work_cb_reference>(w, work_callback_by_reference,ACTION_NOT_DEFINED);
    wrk->do_work();
    printf("%s %d %d\n",__func__, __LINE__, w.a);
}


int main(int argc, char** argv) {

    test_method_create_work();
    test_constructor_work();
    test_args_as_reference();
}