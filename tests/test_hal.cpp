#include "petasense_os_hal.h"

int main(int argc, char** argv) {

    Mutex* my_mutex;
    create_mutex(&my_mutex);

    mutex_lock(my_mutex);
    mutex_unlock(my_mutex);

    destroy_mutex(my_mutex);
}
