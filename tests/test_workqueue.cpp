

/* insert a Workqueue from Workqueue Manager */

/* remove a Workqueue from Workqueue Manager */

/* find a Workqueue with given schedule or parameters */

#include<cstdio>
#include<ctime>
#include<climits>
#include <unistd.h>
#include<vector>
#include<string>
#include<list>
#include <map>


#include "Work.h"
#include "WorkQueue.h"


using std::map;
using std::string;

WorkQueueManager wq_manager;

void test_scheduled(void) {
    // ScheduledWorkQueue my_wq("1-12-30", "2-00-00", 70);
    // wq_manager.add_workqueue(&my_wq);


    // measurement_params m1 ={1, 1234, 0, 0, nullptr};
    // work_args arg1 = (work_args)&m1;


    // measurement_params m2 ={2, 1222, 0, 0, nullptr};
    // work_args arg2 = (work_args)&m2;

    // // auto my_work = new Work(MEASURE_SENSOR, arg, trigger_measure);
    // auto my_work1 = CREATE_WORK(MEASURE_SENSOR, arg1, trigger_measure);
    // auto my_work2 = CREATE_WORK(MEASURE_SENSOR, arg2, trigger_measure);

    // // Work measurement_vib_fb = {MEASURE_SENSOR, (work_args)(&args), (work_func)trigger_measure}
    // my_wq.queue_work(my_work1);
    // my_wq.queue_work(my_work2);

}

void test_timelist(void) {
    // std::list<std::string> tlist={"6-12-00", "1-00-15","1-00-20","2-00-15", "2-00-20", "6-00-15","6-04-15","6-04-30"};
    // TimelistWorkQueue my_time_wq(&tlist);
    // my_time_wq.queue_work(my_work1);
    // my_time_wq.queue_work(my_work2);
    // wq_manager.add_workqueue(&my_time_wq);
    // my_time_wq.enable();
}

void dummy_work_callback(WorkArgs* arg) {
    printf("Dummy Work callback triggered\n");
}

int main(int argc, char** argv) {
    /* Specify the parameters to create a Scheduler-WorkQueue */
    ScheduledWorkQueueArgs args{"0-00-00", "0-00-00", 300};
    /* create the workqueue using factory */
    auto* wq1 = WorkQueueManager::create_workqueue(&args, WorkQueueType::WORKQUEUE_TYPE_SCHEDULE);
    /* add it to the global WorkQueue Manager */
    wq_manager.add_workqueue(wq1);

    /* add some work to the Workqueue */
    WorkArgs dummy;
    auto* wrk = Work::create_work(&dummy, dummy_work_callback, ActionType::ACTION_NOT_DEFINED);
    /* queue the dummy work to Scheduled-Work-Queue */
    wq1->queue_work(wrk);

    /* enable the Workqueue */
    wq1->enable();

    while(1) {
        time_t current = time(0);
        time_t next_due = wq_manager.nextDueTime();
        if (next_due == INT_MAX) {
            printf("No pending Work found(either disabled or Empty) - Go to sleep for 30 seconds by default!!\n");
            sleep(30);
        }
        /* Work is pending */
        else if (next_due <= current) {
            wq_manager.run();
        }
        else {
            printf("%s sleeping for %lu seconds\n", __func__, next_due - current);
            sleep(next_due - current );
        }

        printf("%s Workqueue(handle:%p type:%d) next-due: %lu current:%lu DueTimeInseconds:%d\n",
                __func__, wq1, (int)wq1->getType(), wq1->DueTime(), time(0), wq1->DueTimeInseconds());

        // printf("%s TimelistWorkquee: %d next-due: %lu current:%lu DueTimeInseconds:%d\n", __func__, __LINE__, my_time_wq.DueTime(), time(0), my_time_wq.DueTimeInseconds());
        // printf("%s OneShotWorkquee: %d next-due: %lu current:%lu DueTimeInseconds:%d\n", __func__, __LINE__, instant_wq.DueTime(), time(0), instant_wq.DueTimeInseconds());
    }

#if 0
    my_wq.enable();

    // sleep(10);
    // printf("%s %d next-due: %lu current:%lu DueTimeInseconds:%d\n", __func__, __LINE__, my_wq.DueTime(), time(0), my_wq.DueTimeInseconds());
    auto instant_work1 = CREATE_WORK(P2P_SYNC, 0, trigger_p2p_sync);
    auto instant_work2 = CREATE_WORK(CONNECT_WIFI_AND_REPORT, 0, trigger_wifi_connect);
    wq_manager.add_workqueue(&instant_wq);
    instant_wq.queue_work_delayed(instant_work2, 30); // 30 seconds from now
    instant_wq.queue_work_delayed(instant_work1, 10); // 10 seconds  from now
    instant_wq.enable();

    while(1) {
        time_t current = time(0);
        time_t next_due = wq_manager.nextDueTime();
        printf("%s %d %u\n", __func__, __LINE__, next_due);
        if (next_due <= current) {
            wq_manager.run();
        }
        else {
            printf("%s sleeping for %u seconds\n", __func__, next_due - current);
            sleep(next_due - current );
        }
        printf("%s ScheduledWorkQueue %d next-due: %lu current:%lu DueTimeInseconds:%d\n", __func__, __LINE__, my_wq.DueTime(), time(0), my_wq.DueTimeInseconds());
        printf("%s TimelistWorkquee: %d next-due: %lu current:%lu DueTimeInseconds:%d\n", __func__, __LINE__, my_time_wq.DueTime(), time(0), my_time_wq.DueTimeInseconds());
        printf("%s OneShotWorkquee: %d next-due: %lu current:%lu DueTimeInseconds:%d\n", __func__, __LINE__, instant_wq.DueTime(), time(0), instant_wq.DueTimeInseconds());

    }
#endif

}