#include <ctime>
#include <cstdio>
#include <unistd.h>
// #include <iostream>
#include <string>

#include "timestamp.h"

using std::string;

bool getUtcDate (time_t currentTime, scheduledTime &currentDate)
{
  tm timeStruct{};

  if (nullptr != gmtime_r (&currentTime, &timeStruct)) {
    currentDate.wday = timeStruct.tm_wday;
    currentDate.hour = timeStruct.tm_hour;
    currentDate.min = timeStruct.tm_min;
    currentDate.sec = timeStruct.tm_sec;
    // printf ("%s: %s", __func__, asctime (&timeStruct));

    return true;
  }

  printf ("Failed to get currentDate from timestamp");
  return false;
}

time_t getTimestamp (time_t fromTs, scheduledTime &scheduleTime, ts_dir_t tsDir)
{
  scheduledTime fromTime;
  if (!getUtcDate (fromTs, fromTime)) {
    return 0;
  }

  if (tsDir == TS_DIRECTION_PAST)
  {
    // Earlier on the same day
    if ((scheduleTime.wday == fromTime.wday) && ((scheduleTime.hour < fromTime.hour) ||
                                                 ((scheduleTime.hour == fromTime.hour) &&
                                                  (scheduleTime.min < fromTime.min)))) {
      return fromTs - (((fromTime.hour - scheduleTime.hour) * SECONDS_IN_AN_HOUR) +
                       ((fromTime.min - scheduleTime.min) * SECONDS_IN_A_MINUTE) +
                       (fromTime.sec - scheduleTime.sec));
    }
    // go back a week
    time_t oneWeekAgoTs = fromTs - SECONDS_IN_A_WEEK;

    // find diff in days from last week to schedule day
    int wdayDiff = ((DAYS_IN_A_WEEK + scheduleTime.wday) - fromTime.wday) % DAYS_IN_A_WEEK;

    // +/- hour/min to be automatically handled
    return oneWeekAgoTs + ((wdayDiff * SECONDS_IN_A_DAY) +
                          ((scheduleTime.hour - fromTime.hour) * SECONDS_IN_AN_HOUR) +
                          ((scheduleTime.min - fromTime.min) * SECONDS_IN_A_MINUTE) +
                          (scheduleTime.sec - fromTime.sec));

  } else { // TS_DIRECTION_FUTURE
    // same or later time in the day
    if ((scheduleTime.hour > fromTime.hour) ||
        ((scheduleTime.hour == fromTime.hour) && (scheduleTime.min >= fromTime.min))) {
      // same day will be 0
      int wdayDiff = ((DAYS_IN_A_WEEK + scheduleTime.wday) - fromTime.wday) % DAYS_IN_A_WEEK;
      return fromTs + ((wdayDiff * SECONDS_IN_A_DAY) + ((scheduleTime.hour - fromTime.hour) * SECONDS_IN_AN_HOUR) +
                       ((scheduleTime.min - fromTime.min) * SECONDS_IN_A_MINUTE));
    } else {
      // goto next day zero hour zero min
      time_t tempTs = fromTs + ((HOURS_IN_A_DAY - fromTime.hour) * SECONDS_IN_AN_HOUR) +
                      ((MINS_IN_AN_HOUR - fromTime.min) * SECONDS_IN_A_MINUTE);

      // next day scheduled time
      tempTs = tempTs + (scheduleTime.hour * SECONDS_IN_AN_HOUR) +
               (scheduleTime.min * SECONDS_IN_A_MINUTE);
      int wdayDiff;
      if (scheduleTime.wday == fromTime.wday) {
        // Earlier on the same day
        wdayDiff = DAYS_IN_A_WEEK;
      } else {
        wdayDiff = ((DAYS_IN_A_WEEK + scheduleTime.wday) - fromTime.wday) % DAYS_IN_A_WEEK;
      }

      // (wdayDiff-1) Because we had already moved a day ahead
      return tempTs + ((wdayDiff - 1) * SECONDS_IN_A_DAY);
    }
  }
}

scheduledTime convert_timestring(const string &scheduleTimeStr)
{
    char *conversionStopPtr;
    scheduledTime scheduleTime;

    scheduleTime.wday = strtol (scheduleTimeStr.c_str (), &conversionStopPtr, 10);
    scheduleTime.hour = strtol (conversionStopPtr + 1, &conversionStopPtr, 10);
    scheduleTime.min = strtol (conversionStopPtr + 1, nullptr, 10);
    scheduleTime.sec = 0;
    printf ("%s: wday: %d, hour: %d, min: %d  sec: %d\n", __func__,
        scheduleTime.wday, scheduleTime.hour, scheduleTime.min, scheduleTime.sec);
    return scheduleTime;
}
