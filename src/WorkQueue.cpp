#include <list>
#include <cstdio>
#include <climits>
#include <string>
#include <queue>

#include "Work.h"
#include "WorkQueue.h"
#include "ScheduledWorkQueue.h"
#include "OneshotWorkQueue.h"
#include "TimelistWorkQueue.h"
#include "StateWorkQueue.h"

/* factory method to create workqueues */
WorkQueue* WorkQueueManager::create_workqueue(WorkQueueArgs* p, WorkQueueType qtype) {
    WorkQueue* q = nullptr;
    if (qtype == WorkQueueType::WORKQUEUE_TYPE_SCHEDULE)
    {
        q = new ScheduledWorkQueue((ScheduledWorkQueueArgs*)p);
    }
    else if(qtype == WorkQueueType::WORKQUEUE_TYPE_ONESHOT)
    {
        q = new OneshotWorkQueue();
    }
    else if(qtype == WorkQueueType::WORKQUEUE_TYPE_TIMELIST)
    {
        q = new TimelistWorkQueue((TimelistWorkQueueArgs*)p);
    }
    else if(qtype == WorkQueueType::WORKQUEUE_TYPE_STATEBASED)
    {
        q = new StateWorkQueue((StateWorkQueueArgs*)p);
    } else {

    }
    return q;
}

void WorkQueueManager::add_workqueue(WorkQueue*  wq) {
    wqs.push_back(wq);
}

void WorkQueueManager::remove_workqueue(WorkQueue* wq) {
    for(auto &wqueue: wqs) {
        if (wqueue == wq) {
            printf("Workqueue: %p found\n", wq);
            wqueue->flush();
        }
    }
}

WorkQueue* WorkQueueManager::find_workqueue(WorkQueueArgs* p, WorkQueueType qtype)
{
    for(auto a = wqs.begin(); a!=wqs.end(); a++){
        printf("%s %d\n",__func__, __LINE__);
        /* first match the WorkQueue Type */
        if ((*a)->getType() == qtype) {
            /* then match the Workqueue specific compare() function. can be done better
             * using c++ overloading of operators - but fine for now.
             */
            if((*a)->compare_args(p)) {
                printf("Found Workqueue: %p\n",(*a));
                return (*a);
            }
        }
    }
    return 0;
}

void WorkQueueManager::run(void) {
    for(auto a = wqs.begin(); a!=wqs.end(); a++){
        printf("%s %d\n",__func__, __LINE__);
        (*a)->run();
    }
}

time_t WorkQueueManager::nextDueTime(void) {
    time_t minimum = INT_MAX;
    int i = 0;
    for(auto a = wqs.begin(); a!=wqs.end(); a++){
        printf("%s %d\n",__func__, __LINE__);
        if ( (*a)->isEmpty() || !(*a)->isEnabled()) {
            printf("Worqueue(%p) empty or disabled\n", *a);
            continue;
        }

        int next = (int)(*a)->DueTime();
        if (next < minimum) {
            minimum = next;
        }
    }
    return minimum;
}