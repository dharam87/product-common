/* OS abstraction layer definitions for Petasense common libraries */
#include<cstdio>

#if defined(MBED) || defined(__MBED__)
#include "mbed.h"
#include "rtos.h"
#include "Mutex.h"

#define Mutex Mutex

void mutex_lock(Mutex* mutex) {
    mutex->lock();
}
void mutex_trylock(Mutex* mutex){
    mutex->trylock();
}

void mutex_unlock(Mutex* mutex) {
    mutex->unlock();
}

void destroy_mutex(Mutex* mutex) {
    delete mutex;
}

void create_mutex(Mutex** mutex) {
    *mutex = new Mutex();
}

#elif defined(__linux__)
#include <pthread.h>

#define error_printf printf
#define trace_printf printf
#define spew_printf printf

#define Mutex pthread_mutex_t

void mutex_lock(Mutex* mutex) {

    pthread_mutex_lock(mutex);
}
void mutex_trylock(Mutex* mutex) {
    pthread_mutex_trylock(mutex);
}

void mutex_unlock(Mutex* mutex) {
    pthread_mutex_unlock(mutex);
}

void create_mutex(Mutex** mutex) {

    int ret = pthread_mutex_init(*mutex, NULL);
    if (!ret) {
        printf("Mutex created/Initialized successfully\n");
    }
}

void destroy_mutex(Mutex* mutex) {
    int ret = pthread_mutex_destroy(mutex);
    if (!ret) {
        printf("Mutex destroyed successfully\n");
    }
}

#endif


